import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:wallpaper/constants/sizeconfig.dart';
import 'package:wallpaper/provider/provider_bottom_nav_bar.dart';
import 'package:wallpaper/screens/chat/moneychat.dart';
import 'package:wallpaper/screens/home/products.dart';
import 'package:wallpaper/ui/widgets/appbar_widget.dart';
import 'package:wallpaper/ui/widgets/bottom_nav_bar.dart';

import 'history_body.dart';
import 'history_page.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  List<Widget> listOfPages = [
    ProductsPage(),
    const HistoryPage(),
    const ChatPageMoney(),
  ];
  late ProviderBottomNavBar _bottomNavBar;
  Future<bool?> showwarning(BuildContext context) async => showDialog<bool>(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Выход"),
          content: Text("Вы действительно хотите выходит?"),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text('Назад')),
            ElevatedButton(
                onPressed: () => Navigator.pop(context, true),
                child: Text('Выход'))
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    _bottomNavBar = context.watch();
    SizeConfig().init(context);

    return WillPopScope(
      onWillPop: () async {
        bool? showdialog = await showwarning(context);
        return showdialog ?? false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBArClass(),
        bottomNavigationBar: MyBottomBar(),
        body: listOfPages[_bottomNavBar.currentIndex],
      ),
    );
  }
}
