import 'package:custom_refresh_indicator/custom_refresh_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wallpaper/constants/constants.dart';
import 'package:wallpaper/constants/sizeconfig.dart';
import 'package:wallpaper/core/graphql_clients.dart';
import 'package:wallpaper/core/query.dart';
import 'package:wallpaper/provider/provider_switch.dart';
import 'package:wallpaper/screens/appbar/filterpage.dart';
import 'package:wallpaper/screens/chat/ordercoment.dart';

class HistoryOrderPage extends StatefulWidget {
  @override
  State<HistoryOrderPage> createState() => _HistoryOrderPageState();
}

class _HistoryOrderPageState extends State<HistoryOrderPage> {
  List listOfStrings = ["id", "Дата заказа", "N%", "Артикул", "Статус", 'Чат'];
  List<double> sizeList = [65, 90, 60, 100, 85, 70];

  List listOfRows = ["snapshot.data![index]['node']['id']"];

  String amount = "last:10";

  int st = 0;

  int end = 10;
  List? snapshot;
  List<bool>? listorder;

  @override
  Widget build(BuildContext context) {
    listorder = context.watch<ProviderSvitch>().filterlist;
    List<Widget> row = List.generate(6, (i) {
      if (listorder![i]) {
        return elevatedButtonMethod(context, listOfStrings[i], sizeList[i]);
      } else {
        return Container();
      }
    });
    return Column(
      children: [
        FutureBuilder(
          future: funkorder(),
          builder: (context, AsyncSnapshot snap) {
            if (snap.hasData) {
              snapshot = snap.data;

              return Column(
                children: [
                  Container(
                    height: getHeight(555),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: getWidth(3)),
                          width: getWidth(540),
                          child: Column(
                            children: [
                              Container(
                                color: Colors.white,
                                margin: EdgeInsets.only(left: getWidth(1)),
                                child: Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {},
                                      child: Icon(Icons.delete),
                                      style: ElevatedButton.styleFrom(
                                        fixedSize: Size(
                                          getWidth(50),
                                          getHeight(50),
                                        ),
                                        minimumSize: Size(
                                          getWidth(50),
                                          getHeight(50),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: row,
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              SizedBox(
                                  height: getHeight(500),
                                  child: CustomRefreshIndicator(
                                    onRefresh: () {
                                      setState(() {});
                                      return Future.delayed(
                                          const Duration(seconds: 3));
                                    },
                                    builder: (BuildContext context,
                                        Widget child,
                                        IndicatorController controller) {
                                      return AnimatedBuilder(
                                        animation: controller,
                                        builder: (BuildContext context, _) {
                                          return Stack(
                                            alignment: Alignment.topCenter,
                                            children: <Widget>[
                                              if (!controller.isIdle)
                                                Positioned(
                                                  top: 35.0 * controller.value,
                                                  child: SizedBox(
                                                    height: 30,
                                                    width: 30,
                                                    child:
                                                        CircularProgressIndicator(
                                                      value: !controller
                                                              .isLoading
                                                          ? controller.value
                                                              .clamp(0.0, 1.0)
                                                          : null,
                                                    ),
                                                  ),
                                                ),
                                              Transform.translate(
                                                offset: Offset(0,
                                                    100.0 * controller.value),
                                                child: child,
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                    child: listViewBuilderMethod(snapshot!),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else {
              return Container(
                height: getHeight(555),
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
              onPressed: () {
                if (st > 9) {
                  amount = snapshot!.isEmpty
                      ? "first:10"
                      : "first:10, after:\"${snapshot![snapshot!.length - 1]['cursor']}\"";
                  setState(() {
                    st = st - 10;
                    end = end - 10;
                  });
                }
              },
              child: const Text(
                "<",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: primaryColor),
              ),
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(0),
                  primary: Colors.white,
                  fixedSize: Size(getWidth(15), getWidth(30))),
            ),
            Text(
              "$st/$end",
              style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: primaryColor),
            ),
            ElevatedButton(
              onPressed: () {
                if (snapshot!.length == 10) {
                  amount = "last:10 before:\"${snapshot![0]['cursor']}\"";
                  setState(() {
                    st = st + 10;
                    end = end + 10;
                  });
                }
              },
              child: const Text(
                ">",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: primaryColor),
              ),
              style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  fixedSize: Size(getWidth(20), getWidth(30))),
            ),
          ],
        )
      ],
    );
  }

  ListView listViewBuilderMethod(List<dynamic> snapshot1) {
    List snapshot = snapshot1.reversed.toList();
    return ListView.builder(
      itemBuilder: (context, index) {
        List date = snapshot[index]['node']['dateOrder'].split("T");
        String str1 = date[0];
        String str2 = date[1].split('.')[0];

        return Container(
          height: getHeight(50),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.shade400),
          ),
          child: Row(
            children: [
              IconButton(
                  onPressed: () async {
                    showDialog(
                        context: context,
                        builder: (_) {
                          return AlertDialog(
                            title: Text(
                                "Вы хотите отменить заказ ${snapshot[index]['node']['id']} ?"),
                            actions: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text('Назад')),
                              ElevatedButton(
                                  onPressed: () async {
                                    var res = await clientAll.value
                                        .mutate(MutationOptions(
                                      document: gql(deleteOrdder(
                                          snapshot[index]['node']['id'])),
                                    ));

                                    setState(() {});
                                    Navigator.pop(context);
                                    if (res.data == null) {
                                      const snackBar = SnackBar(
                                        content: Text(
                                          'Вы не можете отменить этот заказ!',
                                          style: TextStyle(color: Colors.red),
                                        ),
                                      );

                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    }
                                  },
                                  child: Text('Отменить'))
                            ],
                          );
                        });
                  },
                  icon: Icon(Icons.delete)),
              listorder![0]
                  ? containerMethod(snapshot, index,
                      snapshot[index]['node']['id'].toString(), 60)
                  : Container(),
              listorder![1]
                  ? containerMethod(
                      snapshot,
                      index,
                      "$str1 \n $str2",
                      110,
                    )
                  : Container(),
              listorder![2]
                  ? containerMethod(
                      snapshot,
                      index,
                      snapshot[index]['node']['amount'].toString(),
                      50,
                    )
                  : Container(),
              listorder![3]
                  ? containerMethod(
                      snapshot,
                      index,
                      snapshot[index]['node']['product']['article'].toString(),
                      110,
                    )
                  : Container(),
              listorder![4]
                  ? containerMethod(
                      snapshot,
                      index,
                      snapshot[index]['node']['status'].toString() == 'WAITING'
                          ? 'Ожидает'
                          : 'Получено',
                      90,
                    )
                  : Container(),
              listorder![5]
                  ? Container(
                      alignment: Alignment.center,
                      height: getHeight(30),
                      width: getWidth(60),
                      child: IconButton(
                        // Chat
                        icon: const Icon(
                          Icons.mode_comment_outlined,
                          size: 20,
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChatPageOrder()));
                        },
                      ),
                    )
                  : Container(),
            ],
          ),
        );
      },
      itemCount: snapshot.length,
    );
  }

  Container containerMethod(
      List<dynamic> snapshot, int index, String name, double widgth) {
    return Container(
      height: getHeight(50),
      width: getWidth(widgth),
      alignment: Alignment.center,
      // id
      child: Text(name),
    );
  }

  ElevatedButton elevatedButtonMethod(
      BuildContext context, String nameOfButton, double width) {
    return ElevatedButton(
      onPressed: () {},
      child: Text(
        nameOfButton,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      style: ElevatedButton.styleFrom(
        fixedSize: Size(
          getWidth(50),
          getHeight(50),
        ),
        minimumSize: Size(
          getWidth(width),
          getHeight(50),
        ),
      ),
    );
  }

  funkorder() async {
    QueryResult? products = await clientAll.value.mutate(MutationOptions(
      document: gql(ordersAllQuery(amount)),
    ));
    print(products.toString());
    final List productlist = products.data?["orders"]['edges'];
    return productlist;
  }
}
